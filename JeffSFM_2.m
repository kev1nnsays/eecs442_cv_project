close all;
clear;
I1 = rgb2gray(imread('A1.jpg'));
I2 = rgb2gray(imread('A2.jpg'));
points1 = detectHarrisFeatures(I1);
points2 = detectHarrisFeatures(I2);
[features1, valid_points1] = extractFeatures(I1, points1);
[features2, valid_points2] = extractFeatures(I2, points2);
indexPairs = matchFeatures(features1, features2);
matchedPoints1 = valid_points1(indexPairs(:, 1), :);
matchedPoints2 = valid_points2(indexPairs(:, 2), :);
x=matchedPoints1.Location;
x1=matchedPoints2.Location;
x(:,3) = 1;
x1(:,3) = 1;
avgerr =0;
%x = x.';
x_xavg = mean(x(:,1));
x_yavg = mean(x(:,2));
dx1 = sum(abs((x(:,1)-x_xavg))/size(x,1));
dy1 = sum(abs((x(:,2)-x_yavg))/size(x,1));
T1 = [1/dx1 0 (-1*x_xavg)/dx1; 0 1/dy1 (-1*x_yavg)/dy1; 0 0 1];
for i = 1:size(x,1)
    xT(i,:) = (T1*x(i,:).').';
end
%x1 = x1.';
x1_xavg = mean(x1(:,1));
x1_yavg = mean(x1(:,2));
dx2 = sum(abs((x1(:,1)-x1_xavg))/size(x1,1));
dy2 = sum(abs((x1(:,2)-x1_yavg))/size(x1,1));
T2 = [1/dx2 0 (-1*x1_xavg)/dx2; 0 1/dy2 (-1*x1_yavg)/dy2; 0 0 1];
for i = 1:size(x1,1)
    x1T(i,:) = (T2*x1(i,:).').';
end
%Y = zeros(1,37) ;
%Y= Y';
A=[xT(:,1).*x1T(:,1),xT(:,1).*x1T(:,2),xT(:,1),xT(:,2).*x1T(:,1),xT(:,2).*x1T(:,2),xT(:,2),x1T(:,1),x1T(:,2),ones(size(xT,1),1)];
%a = inv(A.'*A)*A.'*Y;
[U,S,V] = svd(A);
[m,n] = size(V);
F=[V(1,n) V(2,n) V(3,n) ;V(4,n) V(5,n) V(6,n); V(7,n) V(8,n) V(9,n)];
[U2,S2,V2] = svd(F);
[m2,n2]=size(S2);
S2(:,n2) = zeros(1,m2);
F_=U2*S2*V2';
F_=T1'*F_*T2;
img1 = imread('IMG_20151117_005200.jpg');
img2 = imread('IMG_20151117_005219.jpg');
figure();
colormap('gray');
image(img1);
hold on;
plot(x(:,1),x(:,2),'r.','MarkerSize',20);
for i = 1:size(x1,1)
    line=F_*x1(i,:)';
    avgerr =avgerr+ abs(line(1)*x(i,1)+line(2)*x(i,2)+line(3))/(line(1)^2+line(2)^2)^0.5;
    j = x(i,1)-100:0.01:x(i,1)+100;
    k = (-1*line(3)-line(1)*j)/line(2);
    plot(j,k);
end
figure();
colormap('gray');
image(img2);
hold on;
plot(x1(:,1),x1(:,2),'r.','MarkerSize',20);
for i = 1:size(x,1)
    line=F_'*x(i,:)';
    avgerr =avgerr+ abs(line(1)*x1(i,1)+line(2)*x1(i,2)+line(3))/(line(1)^2+line(2)^2)^0.5;
    j = x1(i,1)-100:0.01:x1(i,1)+100;
    k = (-1*line(3)-line(1)*j)/line(2);
    plot(j,k);
end
avgerr = avgerr/(size(x1,1)+size(x,1))
P1= eye(3);
P1(:,4)=[0;0;0]
e = null(F_);
Ex  =[0 -1*e(3) e(2);e(3) 0 -1*e(1);-1*e(2) e(1) 0];
P2=Ex*F_;
P2(:,4)=e;
P2 =P2/P2(3,4)
[K2 R2]=rq(P2(1:3,1:3));
K2
t2=K2\P2(:,4)
c2 = -R2\t2
WP = zeros(3,size(x,1));
for i=1:size(x,1)
    v1=x(i,:)'/norm(x(i,:)');
    v2=inv(R2)*inv(K2)*x1(i,:)';
    v2=v2/norm(v2);
    Front=[];
    Back=[];
    I=eye(3);
    Front=I -v1*v1'+I-v2*v2';
    Front = inv(Front);
    Back = (I-v2*v2')*c2;
    WP(:,i)=Front*Back;
end
figure();
plot(WP(1,:),WP(2,:),'x');
figure();
plot(WP(1,:),WP(3,:),'x');
figure();
plot(WP(2,:),WP(3,:),'x');