clear;
I1= imread('A1.JPG');
I2 = imread('A2.JPG');
I2 = I2(1:3040, 1:4096, :);
figure
imshowpair(I1, I2, 'montage');
title('Original Images');
load upToScaleReconstructionCameraParameters.mat;
imagePoints1 = detectMinEigenFeatures(rgb2gray(I1), 'MinQuality', 0.1);
figure
imshow(I1, 'InitialMagnification', 50);
title('150 Strongest Corners from the First Image');
hold on
plot(selectStrongest(imagePoints1, 300));
tracker = vision.PointTracker('MaxBidirectionalError', 5, 'NumPyramidLevels', 7);
imagePoints1 = imagePoints1.Location;
initialize(tracker, imagePoints1, I1);
[imagePoints2, validIdx] = step(tracker, I2);
matchedPoints1 = imagePoints1(validIdx, :);
matchedPoints2 = imagePoints2(validIdx, :);
% imshow(I1)
% matchedPoints1 = ginput(8)
% imshow(I2)
% matchedPoints2 = ginput(8)


figure
showMatchedFeatures(I1, I2, matchedPoints1, matchedPoints2);
title('Tracked Features');
[fMatrix, epipolarInliers] = estimateFundamentalMatrix(matchedPoints1, matchedPoints2, 'Method', 'MSAC', 'NumTrials', 10000);
inlierPoints1 = matchedPoints1(epipolarInliers, :);
inlierPoints2 = matchedPoints2(epipolarInliers, :);
figure
showMatchedFeatures(I1, I2, inlierPoints1, inlierPoints2);
title('Epipolar Inliers');
e = null(fMatrix'); 
Ex = [0 -1*e(3) e(2);e(3) 0 -1*e(1); -1*e(2) e(1) 0];
P2 = [-Ex*fMatrix', e];
[M, R] = rq(P2(1:3,1:3)); %need to fix this so we get back a set of M,R then decide which M is the best
t = inv(M)*e
R

