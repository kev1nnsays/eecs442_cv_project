function [R T] = cameraPose(fMatrix, cameraParams)
M= cameraParams.IntrinsicMatrix;
e = null(fMatrix');
Ex = [0 -1*e(3) e(2);e(3) 0 -1*e(1); -1*e(2) e(1) 0];
R = inv(M)*Ex*fMatrix;
T = inv(M)*e;
T = T';
end